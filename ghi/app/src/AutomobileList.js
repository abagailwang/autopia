import React, { useEffect, useState } from 'react';

function AutomobileList({ automobiles, fetchAutomobiles }) {
    /////////////////////load sales://///////////////////////////////
    const [sales, setSales] = useState([])

    async function fetchSales() {
      const url = 'http://localhost:8090/api/sales/';
      const response = await fetch(url);
      const data = await response.json();
      const salesData = data.sales
      setSales(salesData);
  }

  useEffect(() => {
      fetchSales();
    }, []);


////////////////////////find only not sold automobiles///////

  function availableAutos(automobiles, sales)  {
    let autos = []
      for (let object of sales) {
        autos.push(object["automobile"])
      }

      let sold = []
      for (let auto of autos) {
        sold.push(auto["vin"])
      }

      const results = []
      for (let automobile of automobiles) {
        if (sold.includes(automobile["vin"]) != true) {
          results.push(automobile)
        }
      }
      return results;
  }


  ///////////////only display not sold automobiles//////
  automobiles = availableAutos(automobiles, sales)



  return (
    <>

    <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>


            </tr>
          </thead>
          <tbody>
            {automobiles.map(automobile => {
                return (
                    <tr key={automobile.href}>
                        <td>{ automobile.vin }</td>
                        <td>{ automobile.color }</td>
                        <td>{ automobile.year }</td>
                        <td>{ automobile.model.name }</td>
                        <td>{ automobile.model.manufacturer.name }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>
        </>
  )
}

export default AutomobileList;
