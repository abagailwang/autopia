import React, {useEffect, useState} from 'react';

function ManufacturerForm(props) {
    const [name, setName] = useState('');
    const [submitted, setSubmitted] = useState(false);


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }





    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};

        data.name = name;

        console.log(data);

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);

            setName('');
            setSubmitted(true);
            props.fetchManufacturers();
        }

    }




    return (
        <div className="my-5 container">
            <div className="row">
              <div className="col col-sm-auto">
                <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://media3.giphy.com/media/eNshwQyjcZTJqFj50f/giphy.gif?cid=790b7611db463161d3b8e407b16a29f2677d405c8cc268c0&rid=giphy.gif&ct=g" />
              </div>
              <div className="col">
                <div className="card shadow">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                      <h1 className="card-title">Create a manufacturer</h1>
                      <p className="mb-3">
                        Please name your new manufacturer.
                      </p>



                      <div className="row">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} required placeholder="manufacturer name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Manufacturer name</label>
                          </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create manufacturer!</button>
                    </form>
                        {submitted && (
                        <div className="alert alert-success d-none mb-0" id="success-message">
                          Congratulations! Your manufacturer has been saved.
                        </div>
                        )}

                  </div>
                </div>
              </div>
             </div>
          </div>
      );

      }

      export default ManufacturerForm;
