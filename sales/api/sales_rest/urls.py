from django.contrib import admin
from django.urls import path
from .views import sales_persons, sales_person, customers, customer, automobiles, sales, sale

urlpatterns = [
    path("salespersons/", sales_persons, name="sales_persons"),
    path("salespersons/<int:pk>/", sales_person, name="sales_person"),
    path("customers/", customers, name="customers"),
    path("customers/<int:pk>/", customer, name="customer"),
    path("automobiles/", automobiles, name="automobiles"),
    path("sales/", sales, name="sales"),
    path("sales/<int:pk>/", sale, name="sale"),
]
