# CarCar
Team:
* Abbey Wang - Services
* Franky Jiang - Sales


## Skills Utilized
- Front End: React
- Back End: Django, Restful API
- Database: PostgreSQL
>>>>>>> main

## Design
Identification of the Value Objects

CarCar is a website used to manage the sales and services of an inventory of vehicles. Vehicles within the inventory can be utilized within either microservice, whether you want to service the vehicle or you want to sell the vehicle to a customer.

Utilizing restful APIs, we created functionality to create salespersons, customers, and sales within the sales microservice. Utilizing Restful APIs in the services microservice, we are able to create technicians and appointments, and even establish a VIP status.

Overall, the functionality and UI of the website allows us to better care for our cars, or on the contrary, get rid of our cars.


## How to Run this Application

- Clone the Repo inside your terminal
- Open Docker Desktop
- Docker volume create beta-data
- Docker compose build
- docker-compose up


## Inventory Functionality
1. Add new manufacturer
2. add new vehicle model
3. Add new automobile

## Services Functionality

4. Create a new technician
5. Create new service appointment
6. To view all of the upcoming service appointments, click "View appointments". To complete an appointment, click "Finish". To delete an appointment, click "Delete.
New
2:38
7. To look at appointment/service history of a specific vehicle, click on "Service History" and type the VIN number in the search bar. This will show all services for this vehicle, and show whether or not the service has been completed.
8. To determine whether or not an appointment should provide VIP service, view the VIP status column on "View appointments" or on "Service history". The VIP icon will show if it needs VIP treatment.

## CRUD Routes, API Documentation for Services
- CRUD Routes, API Documentation
Appointment Service:

Localhost, Port 8080

POST request to http://localhost:8080/api/technicians/
Request body:
```
{
	"name": "Javier",
  "employee_number": 1
}
```

Returns body:
```
{
	"name": "Javier",
	"employee_number": 1,
	"id": 1
}
```


GET request to http://localhost:8080/api/technicians/
Returns:
```
{
	"technicians": [
		{
			"name": "Javier",
			"employee_number": 1,
			"id": 1
		},
    ]
}
```



POST request to http://localhost:8080/api/appointments/

Request body:
```
{
	"vin": "W3AT3TG3AC1D",
	"customer_name": "Abbey",
	"date_time": "2023-02-02 12:00",
  "technician": "Javier",
	"reason": "tire rotation"
}
```



Returns (status code 200):
```
{
	"href": "/api/appointments/1/",
	"id": 1,
	"vin": "W3AT3TG3AC1D",
	"customer_name": "Abbey",
	"date_time": "2023-02-02 12:00",
	"reason": "tire rotation",
	"completed": false,
	"technician": {
		"name": "Javier",
		"employee_number": 1,
		"id": 1
	}
}
```


GET request to http://localhost:8080/api/appointments/

Returns:
```
{
	"appointments": [
		{
			"href": "/api/appointments/1/",
			"id": 1,
			"vin": "W3AT3TG3AC1D",
			"customer_name": "Abbey",
			"date_time": "2023-02-02T12:00:00+00:00",
			"reason": "tire rotation",
			"completed": false,
			"technician": "Javier"
		},
    ]
}
```



## Sales Functionality
1. Add new sales person
2. Add new customer
3. Create new sale
4. Display a list of all sales made
5. Find sales history tied to a specific sales person

- Restful Endpoints
- Create a Sale/Get a list of Sales endpoint: http://localhost:8090/api/sales/
    Json Body:
```
{
	"sales_person_id": 1,
	"customer_id": 1,
	"automobile_id": 1,
	"price": 1200
}
```

- Create a SalesPerson/List of Sales people endpoint: http://localhost:8090/api/salespersons/
    Json Body:
```
{
	"employee_name": "Rick Chang",
	"employee_number": "1234567",
	"id": 2
}
```

- Create a Customer/List of customers endpoint: http://localhost:8090/api/customers/
    Json Body:
```
{
	"customer_name": "Franky",
	"address": "123 park lane",
	"phone_number": "4234234234"
}
```

## Inventory Functionality
1. Add a manufacturer
2. Add a model
3. Add a automobile
4. Lists for manufacturer, model, automobile

- Resful Endpoints
Create/List a Manufacturer: http://localhost:8100/api/manufacturers/
Json Body:
```
{
	"name": "Mercedes Manu"
}
```

Create/List a Model: http://localhost:8100/api/models/
Json Body:
```
{
	"name": "e350",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer_id": 1
}
```

Create/List a Automobile: http://localhost:8100/api/automobiles/
Json Body:
```
{
	"color": "black",
	"year": "2020",
	"vin": "123456",
	"model_id": 1
}
```
## Diagram

![Context Map](image.png)

## Service microservice
In order to determine whether a customer has VIP status when a service appointment is scheduled for their vehicle, the Services microservice must connect to the Inventory microservices by checking whether the VIN entered in services appointment matches VIN property of automobiles in inventory. Instead of using the VinVO and poller.py to demonstrate this interconnection on the backend, I displayed VIP status on the front end by simply writing a javascript function using the filter method. If you go to ghi/app/src/AppointmentsList.js you will see that first I had to load the inventory microservice's automobile database starting on line 54 by using useState and useEffect. Then, on line 71 I wrote my function vipStatus passing in the parameters of appointment and automobiles to first compose a list of all automobile vins. Then, it returns an image which displays "VIP" if the appointment matching any of the automobile vins is true. When rendering, I made sure to envoke this function on line 117, that way it will show the VIP image if the appointment vin matches automobile vin is true. I believe that displaying VIP status on the front end is easier than the back end because it only requires a simple filter function.

## Sales microservice
Within the Sales microservice, Sales are created through a sales person and customer. Through Django, models are created for sales people and also customers, where restful API's are utilized to create and display all sales people and customers. In order to access the inventory to receive vehicles to sell, I polled for automobiles from the inventory microservice, and utilized it in my sales model to create a sale. I then utilized react frontend to integrate a user friendly interface for creating the customer, sales person, and sale of automobile. Additionally, a page to keep record of all sales created is also made through react. For those who would like to see sales from a specific sales person, a sales history page is also made where you can specify the sales person you want to retrieve sales information from.
